<?php

namespace App\Http\Requests\citoyen;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'string|required',
            'second_name' => 'string|required',
            'birthday_date' => 'date|required',
            'sex' => 'string|required',
            'profession' => 'string|required',
            'diplome' => 'string|required',
            'residence' => 'string|required',
            'telephone' => 'numeric',
            'email' => 'email',
            'photo' => 'file|required',
        ];
    }
}
