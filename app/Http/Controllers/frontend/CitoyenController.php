<?php

namespace App\Http\Controllers\frontend;

use App\Http\Requests\citoyen\StoreRequest;
use App\Models\Citoyen;
use App\Models\Village;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;

class CitoyenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $citoyens = Citoyen::with('village')->get();
        $title = 'Recensements';

        return view('citoyen.frontend.index', compact('citoyens','title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $citoyen = new Citoyen();
        $title = 'recensements';
        $sex = ['homme','femme'];
        $villages = Village::pluck('name','id')->all();
        return view('citoyen.create', compact('citoyen','title','sex','villages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $citoyen = new Citoyen();

        $citoyen->fill($request->except(['_token']));
        if($request->hasFile('photo'))  {


                $photo = $request->file('photo');

                $filename = uniqid(). '.'.$photo->getClientOriginalExtension();

                Image::make($photo)->save(public_path('uploads/avatars/'.$filename));

                $citoyen->photo = $filename;
        }
        if($citoyen->save()) {
            $request->session()->flash('success', 'Vous avez été recensé avec succès.');
            return redirect()->route('recensements.index');
        } else {
            $request->session()->flash('error', 'Echec! Recensement non effectué, veuillez réessayez svp.');
            return back();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Citoyen $citoyen, $id)
    {

        if ($citoyen->findOrFail($id)->delete()) {
            return redirect()->route('recensements.index')->with('message','Citoyen(ne) supprim&eacute; avec succ&egrave;s.');
        }
    }
}
