<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Citoyen;

class ConsultationController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function consultation()
    {
        $citoyens = Citoyen::with('village')->get();
        $title = 'Recensements';

        return view('citoyen.frontend.index', compact('citoyens','title'));
    }
}
