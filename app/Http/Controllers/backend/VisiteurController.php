<?php

namespace App\Http\Controllers\backend;

use App\Http\Requests\visiteur\StoreRequest;
use App\Models\Visiteur;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VisiteurController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $visiteurs = Visiteur::all();

        return view('visiteur.index', compact('visiteurs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $visiteur = new Visiteur();
        $title = 'Consultation';
        return view('visiteur.create', compact('visiteur','title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $visiteur = new Visiteur();

        $visiteur->fill($request->except(['_token']));
        if($visiteur->save()) {
            $request->session()->flash('success', 'Vous avez été enregistré parmi les visiteurs du jour avec succès.');
            return redirect()->route('recensements.index');
        } else {
            $request->session()->flash('error', 'Echec! Enregistrement pour visite non-effectué, veuillez réessayer svp.');
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
