<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Requests\PasswordRequest;

class ProfilController extends Controller
{
    /**
    display password updating form
     */

    public function showPasswordForm()
    {
        return view('auth.passwords.reset');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function updatePassword(PasswordRequest $request, User $user)
    {

        if ($user->where('id', $request->user()->id)->update(['password'=>bcrypt($request->password)])) {

            return redirect()->route('home')->with('message','Mot de passe modifi&eacute; avec succ&egrave;s.');

        };

    }
}
