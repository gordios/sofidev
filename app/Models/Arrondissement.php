<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Arrondissement extends Model
{
    protected $fillable = ['name'];

    public function villages() {
        return $this->hasMany('App\Models\Village');
    }
}
