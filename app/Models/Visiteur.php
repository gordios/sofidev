<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Visiteur extends Model
{
    protected $fillable = [
        'first_name',
        'second_name',
        'telephone',
        'email'
    ];
}
