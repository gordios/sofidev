<?php

namespace App\Models;

use Collective\Html\Eloquent\FormAccessible;
use Illuminate\Database\Eloquent\Model;

class Citoyen extends Model
{
    use FormAccessible;

    protected $dates = ['birthday_date'];

    protected $fillable = [
        'first_name',
        'second_name',
        'birthday_date',
        'sex',
        'profession',
        'diplome',
        'residence',
        'telephone',
        'email',
        'village_id',
        'photo'
    ];

    public function village() {
        return $this->belongsTo('App\Models\Village');
    }

    public function getArrondissementNameAttribute() {
        $arrondissement_id = Village::findOrFail($this->getAttribute('village_id'))->arrondissement_id;
        return Arrondissement::find($arrondissement_id)->name;
    }
}
