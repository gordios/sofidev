<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Arrondissement;

class Village extends Model
{
    protected $fillable = ['name','arrondissement_id'];

    public function arrondissement() {
        return $this->belongsTo('App\Models\Arrondissement');
    }

    public function citoyens() {
        return $this->hasMany('App\Models\Citoyen');
    }
}
