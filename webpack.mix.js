let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Admin Metronic Asset
 |--------------------------------------------------------------------------
 |
 |
 */


/*mix.styles([
    'resources/assets/backend/global/plugins/font-awesome/css/font-awesome.min.css',
    'resources/assets/backend/global/plugins/simple-line-icons/simple-line-icons.min.css',
    'resources/assets/backend/global/plugins/bootstrap/css/bootstrap.min.css',
    'resources/assets/backend/global/plugins/uniform/css/uniform.default.css',
    'resources/assets/backend/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css',
    'resources/assets/backend/global/plugins/datatables/datatables.min.css',
    'resources/assets/backend/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',
    'resources/assets/backend/global/css/jquery.fancybox.min.css',
    'node_modules/bootstrap/dist/css/bootstrap.css',
    'node_modules/sweetalert/dist/sweetalert.css',
    'resources/assets/backend/global/css/components-md.min.css',
    'resources/assets/backend/global/css/plugins-md.min.css',
    'resources/assets/backend/layouts/layout4/css/layout.min.css',
    'resources/assets/backend/layouts/layout4/css/themes/light.min.css',
    'resources/assets/backend/layouts/layout4/css/custom.min.css',
    'resources/assets/backend/global/plugins/select2/css/select2.min.css',
    'resources/assets/backend/global/plugins/select2/css/select2-bootstrap.min.css',
    'resources/assets/backend/admins/pages/css/login.css',
    'resources/assets/backend/admins/layout/css/layout.css',
    'resources/assets/backend/admins/layout/css/themes/default.css',
    'resources/assets/backend/admins/layout/css/custom.css',

], 'public/css/backend.css').sourceMaps();*/


/*mix.scripts([
    'node_modules/jquery/dist/jquery.js',
    'node_modules/sweetalert/dist/sweetalert.min.js',
    'node_modules/laravel-data-method/dist/data-method.js',
    'resources/assets/backend/global/plugins/jquery.min.js',
    'resources/assets/backend/global/plugins/bootstrap/js/bootstrap.min.js',
    'resources/assets/backend/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js',
    'resources/assets/backend/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js',
    'resources/assets/backend/global/plugins/jquery.blockui.min.js',
    'resources/assets/backend/global/plugins/uniform/jquery.uniform.min.js',
    'resources/assets/backend/plugins/bootstrap-switch/js/bootstrap-switch.min.js',
    'resources/assets/backend/pages/scripts/ui-modals.min.js',
    'resources/assets/backend/global/scripts/datatable.js',
    'resources/assets/backend/plugins/datatables/datatables.min.js',
    'resources/assets/backend/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js',
    'resources/assets/backend/global/scripts/app.min.js',
    'resources/assets/backend/global/plugins/jquery-ui/jquery-ui.min.js',
    'resources/assets/backend/pages/scripts/table-datatables-managed.min.js',
    'resources/assets/backend/layouts/layout4/scripts/layout.min.js',
    'resources/assets/backend/layouts/layout4/scripts/demo.min.js',
    'resources/assets/backend/layouts/global/scripts/quick-sidebar.min.js',
    'resources/assets/backend/global/scripts/jquery.fancybox.min.js',
    'resources/assets/backend/global/plugins/select2/js/select2.full.min.js',
    'resources/assets/backend/pages/scripts/components-select2.min.js',
    'resources/assets/backend/global/plugins/jquery-migrate.min.js',
    'resources/assets/backend/global/plugins/jquery.cokie.min.js',
    'resources/assets/backend/global/scripts/metronic.js',
    'resources/assets/backend/admins/layout/scripts/layout.js',
    'resources/assets/backend/admins/layout/scripts/demo.js',
    'resources/assets/backend/admins/pages/scripts/login.js',
    'resources/assets/backend/global/metronicInit.js'

], 'public/js/backend.js').sourceMaps();*/

/*
 |--------------------------------------------------------------------------
 | Mix Frontend Asset
 |--------------------------------------------------------------------------
 |
 |
 */

mix.styles([
    'resources/assets/frontend/vendor/bootstrap/css/bootstrap.min.css',
    'resources/assets/frontend/vendor/font-awesome/css/font-awesome.min.css',
    'resources/assets/frontend/vendor/magnific-popup/magnific-popup.css',
    'resources/assets/frontend/css/creative.min.css',
], 'public/css/frontend.css').sourceMaps();

mix.scripts([
    'resources/assets/frontend/vendor/jquery/jquery.min.js',
    'resources/assets/frontend/vendor/popper/popper.min.js',
    'resources/assets/frontend/vendor/bootstrap/js/bootstrap.min.js',
    'resources/assets/frontend/vendor/jquery-easing/jquery.easing.min.js',
    'resources/assets/frontend/vendor/scrollreveal/scrollreveal.min.js',
    'resources/assets/frontend/vendor/magnific-popup/jquery.magnific-popup.min.js',
    'resources/assets/frontend/js/creative.min.js',
], 'public/js/frontend.js').sourceMaps();

//Frontend image
mix.copy('resources/assets/frontend/img', 'public/img');
//Font-awesome Fonts folder
mix.copy('resources/assets/frontend/vendor/font-awesome/fonts', 'public/fonts');

/*
 |--------------------------------------------------------------------------
 | Mix Custom CSS and JS
 |--------------------------------------------------------------------------
 |
 |
 */

mix.js('resources/assets/js/app.js', 'public/js');
