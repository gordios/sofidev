<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>SOFIDEV</title>

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link rel="stylesheet" href="{{ mix('/css/frontend.css') }}">
</head>

<body id="page-top">

<!-- Navigation -->
@include('partials.frontend.nav')
<!-- header -->
@include('partials.frontend.header.home.header')

@yield('content')

<div class="call-to-action bg-dark">
    <div class="container text-center">
        <h2>Nous vous accompagnons. Faites vous identifier !</h2>
        <a class="btn btn-default btn-xl sr-button" href="{{url('/recensements/create')}}">Rejoignez-nous</a>
    </div>
</div>

<!-- footer -->
@include('partials.frontend.footer.home .footer')

<!-- Scripts -->
<script src="{{ asset('js/frontend.js') }}"></script>

</body>

</html>

