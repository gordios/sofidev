<!DOCTYPE html>

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->

<html lang="fr">

<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8">
    <title>SOFIDEV</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!-- CSRF Token Important-->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link href="{{asset('global/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('global/plugins/simple-line-icons/simple-line-icons.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('global/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('global/plugins/uniform/css/uniform.default.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('global/plugins/bootstrap-switch/css/bootstrap-switch.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{{asset('global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- FANCY BOX CSS -->
    <link href="{{asset('global/css/jquery.fancybox.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- Data Method link-->
    <link rel="stylesheet" href="{{asset('node_modules/bootstrap/dist/css/bootstrap.css')}} ">
    <link rel="stylesheet" href="{{asset('node_modules/sweetalert/dist/sweetalert.css')}} ">
    <!-- Data Method script-->
    <script src="{{asset('node_modules/jquery/dist/jquery.js')}}" type="text/javascript"></script>
    <script src="{{asset('node_modules/sweetalert/dist/sweetalert.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('node_modules/laravel-data-method/dist/data-method.js')}}" type="text/javascript"></script>
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="{{asset('global/css/components-md.min.css')}}" rel="stylesheet" id="style_components" type="text/css" />
    <link href="{{asset('global/css/plugins-md.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="{{asset('layouts/layout4/css/layout.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('layouts/layout4/css/themes/light.min.css')}}" rel="stylesheet" type="text/css" id="style_color" />
    <link href="{{asset('layouts/layout4/css/custom.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- END THEME LAYOUT STYLES -->

    {{--COMBOBOX SEARCH CSS--}}
    <link href="{{asset('global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet" type="text/css" />


    <link rel="shortcut icon" href="{{asset('favicon.ico')}}" />

    {{--print style css--}}
    <style type="text/css">
        @media print
        {
            #statisticDashboard {display: none;}
            table td:last-child {display:none}
            table th:last-child {display:none}

            .ajout, .outils {

                display: none;

            }

            .dataTables_length, .dataTables_filter {

                display: none;

            }
        }
    </style>

    <script src="{{asset('global/plugins/jquery.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('global/plugins/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
</head>
<!-- END HEAD -->

<body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">

<!-- BEGIN HEADER -->
@include('partials.backend.header')
<!-- END HEADER -->

<!-- BEGIN HEADER & CONTENT DIVIDER -->
<div class="clearfix"> </div>
<!-- END HEADER & CONTENT DIVIDER -->
<!-- BEGIN CONTAINER -->
<div class="page-container">

    <!-- BEGIN SIDEBAR -->
@include('partials.backend.menu')
<!-- END SIDEBAR -->

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE BASE CONTENT -->

            <!-- BEGIN STATISTIC CONTENT -->
        {{--@include('partials.backend.statistic')--}}
        <!-- END STATISTIC CONTENT -->

            <div class="row">
                <div class="col-md-12">

                    @yield('content')

                </div>

            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->

<!-- BEGIN FOOTER -->
@include('partials.backend.footer')
<!-- END FOOTER -->


<!--[if lt IE 9]>
<script src="{{asset('global/plugins/respond.min.js')}}" type="text/javascript"></script>
<script src="{{asset('global/plugins/excanvas.min.js')}}" type="text/javascript"></script>
<![endif]-->
<!-- BEGIN CORE PLUGINS -->
<script src="{{asset('global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js')}}" type="text/javascript"></script>
<script src="{{asset('global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
<script src="{{asset('global/plugins/jquery.blockui.min.js')}}" type="text/javascript"></script>
<script src="{{asset('global/plugins/uniform/jquery.uniform.min.js')}}" type="text/javascript"></script>
<script src="{{asset('global/plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}" type="text/javascript"></script>
<script src="{{asset('pages/scripts/ui-modals.min.js')}}" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{asset('global/scripts/datatable.js')}}" type="text/javascript"></script>
<script src="{{asset('global/plugins/datatables/datatables.js')}}" type="text/javascript"></script>
<script src="{{asset('global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="{{asset('global/scripts/app.min.js')}}" type="text/javascript"></script>
<script src="{{asset('global/plugins/jquery-ui/jquery-ui.min.js')}}" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{asset('pages/scripts/table-datatables-managed.min.js')}}" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="{{asset('layouts/layout4/scripts/layout.min.js')}}" type="text/javascript"></script>
<script src="{{asset('layouts/layout4/scripts/demo.min.js')}}" type="text/javascript"></script>
<script src="{{asset('layouts/global/scripts/quick-sidebar.min.js')}}" type="text/javascript"></script>
<!-- END THEME LAYOUT SCRIPTS -->
<!-- FANCY BOX JS -->
<script src="{{asset('global/scripts/jquery.fancybox.min.js')}}" type="text/javascript"></script>


{{--COMBOBOX SEARCH JS--}}
<script src="{{asset('global/plugins/select2/js/select2.full.min.js')}}" type="text/javascript"></script>
<script src="{{asset('pages/scripts/components-select2.min.js')}}" type="text/javascript"></script>

</body>

</html>