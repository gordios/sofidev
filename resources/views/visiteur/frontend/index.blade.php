@extends('layouts.backend')
@section('content')



    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-dark">
                <i class="icon-settings font-dark"></i>
                <span class="caption-subject bold uppercase"> Registre des citoyens et citoyennes recensés. </span>
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-toolbar">
                <div class="row">
                    @if (Session::has('message'))
                        <div class="alert alert-success">
                            <button class="close" data-close="alert"></button>
                            {{Session::get('message')}}
                        </div>
                    @endif

                    @if (Session::has('error'))
                        <div class="alert alert-danger">
                            <button class="close" data-close="alert"></button>
                            {{Session::get('error')}}
                        </div>
                    @endif
                    <div class="col-md-6 ajout">
                        <div class="btn-group">
                            <a id="sample_editable_1_new" class="btn sbold green" href="{{route('home')}}"> Accueil
                                <i class="fa fa-home"></i>
                            </a>

                        </div>
                    </div>
                    
                    <div class="col-md-6 outils">
                        <div class="btn-group pull-right">
                            <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Outils
                                <i class="fa fa-angle-down"></i>
                            </button>
                            <ul class="dropdown-menu pull-right">
                                <li>
                                    <a href="javascript:;" onclick="window.print()">
                                        <i class="fa fa-print"></i> imprimer </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div style="min-height: .01%; overflow-x: auto; width: 100%;">
                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                    <thead>
                    <tr>
                        <th class="text-center"> # </th>
                        <th class="text-center"> Nom </th>
                        <th class="text-center"> Prénom </th>
                        <th class="text-center"> Naissance </th>
                        <th class="text-center"> Sexe </th>
                        <th class="text-center"> Profession</th>
                        {{--<th class="text-center"> Diplome </th>--}}
                        <th class="text-center"> Residence </th>
                        <th class="text-center"> téléphone </th>
                        <th class="text-center"> Email </th>
                        <th class="text-center"> Village </th>
                        <th class="text-center"> Photo </th>
                        @if(\Illuminate\Support\Facades\Auth::user()->admin)
                            <th class="text-center"> Action </th>
                        @endif
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($citoyens as $citoyen)
                        <tr class="odd gradeX">
                            <td class="text-center" style="display: table-cell; vertical-align: middle"> {{$citoyen->id}} </td>
                            <td class="text-center" style="display: table-cell; vertical-align: middle"> {{$citoyen->first_name}} </td>
                            <td class="text-center" style="display: table-cell; vertical-align: middle"> {{$citoyen->second_name}} </td>
                            <td class="text-center" style="display: table-cell; vertical-align: middle"> {{$citoyen->birthday_date->format('d/m/y')}} </td>
                            <td class="text-center" style="display: table-cell; vertical-align: middle"> {{$citoyen->sex == 0 ? 'homme' : 'femme'}} </td>
                            <td class="text-center" style="display: table-cell; vertical-align: middle"> {{$citoyen->profession}} </td>
                            {{--<td class="text-center" style="display: table-cell; vertical-align: middle"> {{$citoyen->diplome}} </td>--}}
                            <td class="text-center" style="display: table-cell; vertical-align: middle"> {{$citoyen->residence}} </td>
                            <td class="text-center" style="display: table-cell; vertical-align: middle"> {{$citoyen->telephone}} </td>
                            <td class="text-center" style="display: table-cell; vertical-align: middle"> {{$citoyen->email}} </td>
                            <td class="text-center" style="display: table-cell; vertical-align: middle"> {{$citoyen->village->name}} </td>
                            <td class="text-center" style="display: table-cell; vertical-align: middle">
                                @if($citoyen->photo)


                                    <a href="{{url('uploads/avatars/'.$citoyen->photo)}}"
                                       data-fancybox="gallery"
                                       data-title="Photo de {{$citoyen->first_name}} {{$citoyen->second_name}}">

                                        <img src="{{url('uploads/avatars/'.$citoyen->photo)}}" style="width: 120px; height: 120px;" alt="">

                                    </a>
                                @else
                                    Aucune photo
                                @endif

                            </td>
                            @if(\Illuminate\Support\Facades\Auth::user()->admin)
                                <td class="text-center" style="display: table-cell; vertical-align: middle">

                        <span >
                            <a class="btn btn-xs btn-danger tooltips"
                               href="/recensements/{{$citoyen->id}}"
                               data-method="DELETE"
                               data-confirm="&Ecirc;tes vous s&ugrave;r de vouloir supprimer le / la citoyen(ne) {{$citoyen->first_name .' '. $citoyen->second_name}} du régistre ?"
                               data-title="Confirmation de suppression"
                               data-theme="bootstrap"
                               data-params='{"id": {{$citoyen->id}}}'
                               data-original-title="Supprimer"
                               data-placement="top"
                               data-container ="body">Supprimer
                            </a>

                        </span>
                                </td>
                            @endif
                        </tr>

                    @endforeach
                    </tbody>
                </table>
            </div>

        </div>
    </div>

@stop