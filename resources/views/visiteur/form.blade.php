{!! Form::model($visiteur, ['route' => $route,  'method' => $method]) !!}
<fieldset>
    <legend>Vos informations:</legend>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('first_name', 'Nom') !!}
                {!! Form::text('first_name', null, ['placeholder' => 'Nom', 'class' => 'form-control', 'required']) !!}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('second_name', 'Prénom') !!}
                {!! Form::text('second_name', null, ['placeholder' => 'Prénom', 'class' => 'form-control', 'required']) !!}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('telephone', 'Telephone') !!}
                {!! Form::text('telephone', null, ['placeholder' => 'telephone','class' => 'form-control']) !!}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('email', 'Email') !!}
                {!! Form::email('email', null, ['placeholder' => 'email','class' => 'form-control']) !!}
            </div>
        </div>
    </div>
</fieldset>
{!! Form::submit($visiteur->id ? 'Update' : 'Continuer', ['class' => 'btn btn-lg btn-primary']) !!}
{!! Form::close() !!}
