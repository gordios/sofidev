@extends('layouts.page')

@section('content')
    <h3>Nouveau visiteur</h3>

    @include('visiteur.form', ['route' => ['visiteurs.store'], 'method' => 'post'])
@endsection
