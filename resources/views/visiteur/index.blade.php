@extends('layouts.backend')
@section('content')



    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-dark">
                <i class="icon-settings font-dark"></i>
                <span class="caption-subject bold uppercase"> Registre des visiteurs enregistrés. </span>
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-toolbar">
                <div class="row">

                    <div class="col-md-6 ajout">
                        <div class="btn-group">
                            <a id="sample_editable_1_new" class="btn sbold green" href="{{route('home')}}"> Accueil
                                <i class="fa fa-home"></i>
                            </a>

                        </div>
                    </div>

                    <div class="col-md-6 outils">
                        <div class="btn-group pull-right">
                            <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Outils
                                <i class="fa fa-angle-down"></i>
                            </button>
                            <ul class="dropdown-menu pull-right">
                                <li>
                                    <a href="javascript:;" onclick="window.print()">
                                        <i class="fa fa-print"></i> imprimer </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div style="min-height: .01%; overflow-x: auto; width: 100%;">
                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                    <thead>
                    <tr>
                        <th class="text-center"> # </th>
                        <th class="text-center"> Nom </th>
                        <th class="text-center"> Prénom </th>
                        <th class="text-center"> téléphone </th>
                        <th class="text-center"> Email </th>
                        <th class="text-center"> Dernière visite </th>
                        <th class="text-center"> Heure de visite </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($visiteurs as $visiteur)
                        <tr class="odd gradeX">
                            <td class="text-center" style="display: table-cell; vertical-align: middle"> {{$visiteur->id}} </td>
                            <td class="text-center" style="display: table-cell; vertical-align: middle"> {{$visiteur->first_name}} </td>
                            <td class="text-center" style="display: table-cell; vertical-align: middle"> {{$visiteur->second_name}} </td>
                            <td class="text-center" style="display: table-cell; vertical-align: middle"> {{$visiteur->telephone}} </td>
                            <td class="text-center" style="display: table-cell; vertical-align: middle"> {{$visiteur->email}} </td>
                            <td class="text-center" style="display: table-cell; vertical-align: middle"> {{$visiteur->created_at->format('d/m/y')}} </td>
                            <td class="text-center" style="display: table-cell; vertical-align: middle"> {{$visiteur->created_at->format('H:m')}} </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

        </div>
    </div>

@stop