<header class="masthead" style="height: 50% !important; min-height: 300px !important;">
    <div style="position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    background: rgba(0, 0, 0, 0.47);"></div>
    <div class="header-content">
        <div class="header-content-inner">
            <h1 id="homeHeading">{!! $title !!}</h1>
        </div>
    </div>
</header>