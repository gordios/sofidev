<header class="masthead">
    <div style="position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    background: rgba(0, 0, 0, 0.47);"></div>
    <div class="header-content">
        <div class="header-content-inner">
            <h1 id="homeHeading">Un citoyen, un arrondissement, un recensement.</h1>
            <hr>
            <p>Bienvenue sur le site officiel de SOFIDEV.</p>
            <a class="btn btn-primary btn-xl js-scroll-trigger" href="#about">En savoir plus</a>
        </div>
    </div>
</header>