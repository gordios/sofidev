<div class="call-to-action bg-dark">
    <div class="container text-center">
        <div class="row">
            <div class="col-lg-4 text-center">
                <i class="fa fa-phone fa-3x sr-contact"></i>
                <p>123-456-6789</p>
            </div>
            <div class="col-lg-4 text-center">
                <i class="fa fa-home fa-3x sr-contact"></i>
                <p>
                    <a href="/" style="color: white;">SOFIDEV</a>
                </p>
            </div>
            <div class="col-lg-4 text-center">
                <i class="fa fa-envelope-o fa-3x sr-contact"></i>
                <p>
                    <a href="#" style="color: white;">contact@sofidev.com</a>
                </p>
            </div>
        </div>
    </div>
</div>