<section id="contact">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 mx-auto text-center">
                <h2 class="section-heading">Contactez-nous</h2>
                <hr class="primary">
                <p>Nous sommes disponible pour toute vos préocupations.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 ml-auto text-center">
                <i class="fa fa-phone fa-3x sr-contact"></i>
                <p>123-456-6789</p>
            </div>
            <div class="col-lg-4 mr-auto text-center">
                <i class="fa fa-envelope-o fa-3x sr-contact"></i>
                <p>
                    <a href="#">contact@sofidev.com</a>
                </p>
            </div>
        </div>
    </div>
</section>