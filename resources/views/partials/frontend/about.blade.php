<section id="about" class="bg-primary">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 mx-auto text-center">
                <h2 class="section-heading text-white">Qu'est ce que SOFIDEV ?</h2>
                <hr class="light">
                <p class="text-white"><strong>SOFIDEV</strong> se positionne comme une ONG d'influence au profit
                  d'une gestion responsable des communautés pour la prise
                  en compte des aspirations des catégories souvent
                  marginalisées de la population, pour le développement des secteurs porteurs de l'économie locale
                  ,des innovations technologique et des services énergétiques
                  propres ayant un impact visible sur la vie des populations cibles. </p>
                  <li class="text-white text-left">Soutien des initiatives</li>

                  <li class="text-white text-left">Renforcement de l'esprit de solidarite</li>

                  <li class="text-white text-left mb-4">Developpement des modeles et mecanismes financiers inclusifs et durable</li>

                <a class="btn btn-default btn-xl js-scroll-trigger" href="{{url('/recensements/create')}}">S'inscrire</a>
            </div>
        </div>
    </div>
</section>
