<div class="page-footer">
    <div class="page-footer-inner"> 2017 &copy; SOFIDEV.
        <a href="/" title="Visitez notre site web" target="_blank">Le recensement pour tous !</a>
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>