
    <div class="page-sidebar-wrapper">
        <!-- BEGIN SIDEBAR -->
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <div class="page-sidebar navbar-collapse collapse">
            <!-- BEGIN SIDEBAR MENU -->
            <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
            <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
            <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
            <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
            <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
            <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
            <ul class="page-sidebar-menu   " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                <li class="nav-item start {{ active('home') }} ">
                    <a href="{{ route('home') }}" class="nav-link nav-toggle">
                        <i class="icon-home"></i>
                        <span class="title">Retour à l'accueil</span>
                    </a>
                </li>
                <li class="heading">
                    <h3 class="uppercase">Options</h3>
                </li>
              {{--  <li class="nav-item  {{ active('budget.*') }}">
                    <a href="#" class="nav-link nav-toggle">
                        <i class="icon-wallet"></i>
                        <span class="title">Budgets</span>
                    </a>
                </li>
                <li class="nav-item  {{ active('depense.*') }}">
                    <a href="#" class="nav-link nav-toggle">
                        <i class="icon-bar-chart"></i>
                        <span class="title">Depenses</span>
                    </a>
                </li>--}}
                @if(\Illuminate\Support\Facades\Auth::user()->admin)
                <li class="nav-item  {{ active('visiteurs.*') }}">
                    <a href="{{ route('visiteurs.index') }}" class="nav-link nav-toggle">
                        <i class="icon-user"></i>
                        <span class="title">Visiteurs</span>
                    </a>
                </li>
                @endif
                <li class="nav-item {{ active('recensements.*') }}">
                    <a href="{{ route('recensements.index') }}" class="nav-link nav-toggle">
                        <i class="icon-briefcase"></i>
                        <span class="title">Recensements</span>
                    </a>
                </li>
            </ul>
            <!-- END SIDEBAR MENU -->
        </div>
        <!-- END SIDEBAR -->
    </div>


