<div class="row" id="statisticDashboard">
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat2 bordered">
            <div class="display">
                <div class="number">
                    <h3 class="font-green-sharp">
                        <span data-counter="counterup" data-value="7800">{{ ($statistic) ? $statistic->budgetRestant : 0 }}</span>
                        <small class="font-green-sharp">F</small>
                    </h3>
                    <small>BUDGET ACTUEL</small>
                </div>
                <div class="icon">
                    <i class="icon-pie-chart"></i>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat2 bordered">
            <div class="display">
                <div class="number">
                    <h3 class="font-red-haze">
                        <span data-counter="counterup" data-value="1349">{{ ($statistic) ? $statistic->reglementTotal : 0 }}</span>
                        <small class="font-red-haze">F</small>
                    </h3>
                    <small>REGLEMENTS / Mois</small>
                </div>
                <div class="icon">
                    <i class="icon-bar-chart"></i>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat2 bordered">
            <div class="display">
                <div class="number">
                    <h3 class="font-blue-sharp">
                        <span data-counter="counterup" data-value="1349">{{ ($statistic) ? $statistic->achatTotal : 0 }}</span>
                        <small class="font-blue-sharp">F</small>
                    </h3>
                    <small>ACHATS / Mois</small>
                </div>
                <div class="icon">
                    <i class="icon-basket-loaded"></i>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat2 bordered">
            <div class="display">
                <div class="number">
                    <h3 class="font-purple-soft">
                        <span data-counter="counterup" data-value="1349">{{ ($statistic) ? $statistic->reliquat : 0 }}</span>
                        <small class="font-purple-soft">F</small>
                    </h3>
                    <small>RELIQUATS </small>
                </div>
                <div class="icon">
                    <i class="icon-cup"></i>
                </div>
            </div>
        </div>
    </div>
</div>
