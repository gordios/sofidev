@extends('layouts.page')

@section('content')
    <h3>Nouveau recensement</h3>

    @include('citoyen.form', ['route' => ['recensements.store'], 'method' => 'post'])
@endsection
