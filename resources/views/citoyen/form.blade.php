{!! Form::model($citoyen, ['route' => $route, 'files'=>true,  'method' => $method]) !!}
<fieldset>
    <legend>Vos informations:</legend>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('first_name', 'Nom') !!}
                {!! Form::text('first_name', null, ['placeholder' => 'Nom', 'class' => 'form-control', 'required']) !!}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('second_name', 'Prénom') !!}
                {!! Form::text('second_name', null, ['placeholder' => 'Prénom', 'class' => 'form-control', 'required']) !!}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('birthday_date', 'Date de naissance') !!}
                {!! Form::date('birthday_date', null, ['class' => 'form-control', 'required']) !!}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('sex', 'Sexe') !!}
                {!! Form::select('sex', $sex, null, ['class' => 'form-control', 'required']) !!}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('Profession', 'Profession') !!}
                {!! Form::text('profession', null, ['placeholder' => 'Profesion','class' => 'form-control', 'required']) !!}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('diplome', 'Diplome') !!}
                {!! Form::text('diplome', null, ['placeholder' => 'Diplome','class' => 'form-control', 'required']) !!}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('residence', 'Residence') !!}
                {!! Form::text('residence', null, ['placeholder' => 'residence','class' => 'form-control','required']) !!}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('telephone', 'Telephone') !!}
                {!! Form::text('telephone', null, ['placeholder' => 'telephone','class' => 'form-control']) !!}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('email', 'Email') !!}
                {!! Form::email('email', null, ['placeholder' => 'email','class' => 'form-control']) !!}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('village_id', 'Village') !!}
                {!! Form::select('village_id', $villages, null, ['class' => 'form-control', 'required']) !!}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('photo', 'Photo') !!}
                {!! Form::file('photo', ['accept' => 'image/*']) !!}
            </div>
        </div>
    </div>
</fieldset>
{!! Form::submit($citoyen->id ? 'Update' : 'Enregistrer', ['class' => 'btn btn-lg btn-primary']) !!}
{!! Form::close() !!}
