@extends('layouts.auth')

@section('form')

    {!! Form::open(['action' => 'Auth\RegisterController@register', 'class' => 'register-form', 'method' => 'post', 'enctype'=>'multipart/form-data']) !!}
    <h3>Nouveau Membre</h3>
    <p class="hint">
        Entrer vos informations personnelles ici:
    </p>

    @if ($errors->has('name'))
        <div class="alert alert-danger">
            <button class="close" data-close="Alerte"></button>
            <span>{{ $errors->first('name') }}</span>
        </div>
    @endif

    <div class="form-group">
        {!! Form::label('name', 'Nom et Pr&eacute;nom', ['class'=>'control-label visible-ie8 visible-ie9']) !!}
        {!! Form::text('name', null, ['class'=>'form-control form-control-solid placeholder-no-fix', 'placeholder' =>'Nom et pr&eacute;nom', 'required'=>'true', 'autofocus'=>'true', 'autocomplete' => 'false', 'id'=>'name']) !!}
    </div>

    @if ($errors->has('email'))
        <div class="alert alert-danger">
            <button class="close" data-close="Alerte"></button>
            <span>{{ $errors->first('email') }}</span>
        </div>
    @endif

    <div class="form-group">
        {!! Form::label('email', 'Email', ['class'=>'control-label visible-ie8 visible-ie9']) !!}
        {!! Form::email('email', null, ['class'=>'form-control form-control-solid placeholder-no-fix', 'placeholder'=>'Entrer votre email', 'required'=>'true', 'autofocus'=>'true', 'autocomplete' => 'false', 'id'=>'email']) !!}
    </div>

    @if ($errors->has('password'))
        <div class="alert alert-danger">
            <button class="close" data-close="Alerte"></button>
            <span>{{ $errors->first('password') }}</span>
        </div>
    @endif

    <div class="form-group">
        {!! Form::label('password', 'Mot de passe', ['class'=>'control-label visible-ie8 visible-ie9']) !!}
        {!! Form::password('password', ['class'=>'form-control form-control-solid placeholder-no-fix', 'autocomplete'=>'off', 'placeholder'=>'Entrer votre mot de passe', 'required'=>'true', 'id'=>'password']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('password-confirm', 'Retapez votre Mot de passe', ['class'=>'control-label visible-ie8 visible-ie9']) !!}
        {!! Form::password('password_confirmation', ['class'=>'form-control form-control-solid placeholder-no-fix', 'autocomplete'=>'off', 'placeholder'=>'Retapez votre mot de passe', 'required'=>'true', 'id'=>'password-confirm']) !!}
    </div>

    {{--@if(auth()->check() && auth()->user()->admin)
    <div class="form-group">
        {!! Form::label('admin', 'Administateur', ['class'=>'control-label visible-ie8 visible-ie9']) !!}
        <select name="admin" id="select2-button-addons-multiple-input-group-sm" class="form-control select2" >
            <option value=0>Utilisateur</option>
            <option value=1>Administrateur</option>
        </select>
    </div>
    @endif--}}

    <div class="form-actions">
        {!! link_to_route('login','Retour', null, ['class'=>'btn btn-default', 'id' => 'register-back-btn']) !!}
        {!! Form::submit('Connexion', ['class'=>'btn btn-success uppercase pull-right', 'id'=> 'register-submit-btn']) !!}
    </div>
    {!! Form::close()!!}
@endsection
