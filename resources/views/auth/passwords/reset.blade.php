@extends('layouts.auth')

@section('form')



    {!! Form::open(['action' => '\App\Http\Controllers\ProfilController@updatePassword', 'class' => 'login-form']) !!}
    <h3 class="form-title">Mot de passe</h3>

    @if ($errors->has('password'))
        <div class="alert alert-danger">
            <button class="close" data-close="Alerte"></button>
            <span>{{ $errors->first('password') }}</span>
        </div>
    @endif

    <div class="form-group">
        {!! Form::label('password', 'Mot de passe', ['class'=>'control-label visible-ie8 visible-ie9']) !!}
        {!! Form::password('password', ['class'=>'form-control form-control-solid placeholder-no-fix', 'autocomplete'=>'off', 'placeholder'=>'Entrer votre nouveau mot de passe', 'required'=>'true', 'id'=>'password']) !!}
    </div>

    @if ($errors->has('password_confirmation'))
        <span class="help-block">
        <strong>{{ $errors->first('password_confirmation') }}</strong>
    </span>
    @endif

    <div class="form-group">
        {!! Form::label('password-confirm', 'Retapez votre Mot de passe', ['class'=>'control-label visible-ie8 visible-ie9']) !!}
        {!! Form::password('password_confirmation', ['class'=>'form-control form-control-solid placeholder-no-fix', 'autocomplete'=>'off', 'placeholder'=>'Retapez votre nouveau mot de passe', 'required'=>'true', 'id'=>'password-confirm']) !!}
    </div>

    <div class="form-actions">
        {{link_to_route('home','Annuler',null,['class'=>'btn btn-default pull-left'])}}
        {!! Form::submit('changer', ['class'=>'btn btn-success uppercase pull-right']) !!}
    </div>

    {!! Form::close()!!}

@endsection
