@extends('layouts.home')

@section('content')
    <!-- about -->
    @include('partials.frontend.about')
    <!-- devise -->
    @include('partials.frontend.devise')
    <!-- portfolio -->
    @include('partials.frontend.portfolio')
@endsection