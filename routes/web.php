<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
})->name('home');
//Route::get('/consultation{token}','ConsultationController@accessConsultaion');
Auth::routes();

Route::group(
    ['middleware' => ['auth']],
    function() {
        Route::group(['namespace' => 'frontend'], function () {
            Route::resource('recensements', 'CitoyenController', ['only' => ['index','create','store','destroy']]);
            Route::get('consultations', 'ConsultationController@consultation')->name('consultations.index');
        });
        Route::resource('visiteurs', 'VisiteurController', ['only' => ['index']]);

    });


Route::group(
    ['namespace' => 'backend'],
    function() {
        Route::resource('visiteurs', 'VisiteurController', ['only' => ['create','index','store']]);
        Route::get('admin', 'DashboardController@admin');
    });

Route::get('password', 'ProfilController@showPasswordForm');
Route::post('/password/update', [
    'as'=>'password.update',
    'uses'=>'ProfilController@updatePassword'
]);
